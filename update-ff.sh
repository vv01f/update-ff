#!/bin/bash
#set u+x
#~ versionno(){
	#~ e=$1
	#~ s=0
	#~ l=0
	#~ for i in $(echo ${e}|tr . " "); do
		#~ s=$(( ${i} * 100 ** ( 3 - ${l} ) + ${s} ))
		#~ l=$(( ${l} + 1 ))
	#~ done
	#~ echo ${s}
#~ }
url="https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US"
dwldir=~/Downloads/
cd ${dwldir}
ffbin=$(find /opt/ -type f|grep "/firefox$"|head -1)
# test with HEAD (-I) request for url redirect, taken from
#   https://stackoverflow.com/questions/3074288/get-url-after-redirect
which curl >/dev/null && url_effective=$( curl -sLI -o /dev/null -w %{url_effective} ${url} ) || { echo "tool missing: curl" && exit 1; }
echo "Downlaod would be: "${url_effective}
next_version=$(echo $url_effective|rev|cut -d/ -f1|cut -d. -f3-|rev|cut -d- -f2)
curr_version=$(firefox -v|cut -d" " -f3)
opti_version=$(${ffbin} -v|cut -d" " -f3)
dwlfn=FirefoxSetup${next_version}.tar.bz2
if test $? -ne 0; then opti_version=0; fi
echo "remote : "${next_version}
echo "local  : "${curr_version}
echo "alt/opt: "${opti_version}
#echo "Current Version: "${curr_version}
#echo "Download Version: "${next_version}
# prepare numbers
#c=$( versionno ${curr_version} )
#n=$( versionno ${next_version} )
#o=$( versionno ${opti_version} )
xfn="/usr/lib/firefox-esr/firefox-esr"
if test ${curr_version} != ${opti_version} #-o ${c} -lt ${o} -a ${o} -ge ${n};
then
	test -x ${xfn} && {
		echo "fixing link…"
		sudo mv ${xfn} ${xfn}.old
		sudo ln -s ${ffbin} ${xfn}
	} || echo "executable missing: "${xfn}
#	test $? -eq 0 && exit 0
fi
# stop if version is not newer
if test ${next_version} = ${opti_version} #${n} -le ${c};
then
	echo "No Update!"
#	test $? -eq 0 && exit 1
else
# get for new version
sudo wget -c -O ${dwldir}${dwlfn} ${url_effective}
err=$?
echo -n "wget return code: "$err
sudo rm -rf /opt/firefox/
sudo mkdir -p /opt/firefox/
err=$?
echo -n "mkdir return code: "$err
sudo tar xfvj ${dwldir}${dwlfn} -C /opt/
# used
# https://linuxconfig.org/how-to-install-latest-firefox-browser-on-debian-9-stretch-linux
sudo mv /usr/lib/firefox-esr/firefox-esr /usr/lib/firefox-esr/firefox-esr.old
sudo ln -s /opt/firefox/firefox /usr/lib/firefox-esr/firefox-esr
fi
